# TaxiWapp Conductor

## Project based on [Ionic](https://github.com/ionic-team/ionic)

## Install

```sh
$ git clone git@gitlab.com:jcaraballos/taxiwapp-conductor.git
cd taxiwapp-conductor
```
## Packages

Install dependencies with [npm](https://www.npmjs.com/)

```sh
$ npm install
```

or with [yarn](https://yarnpkg.com)

```sh
$ yarn
```

## api

[http://app.exitum.us:9080/mocha](http://app.exitum.us:9080/mocha)

## New Service

use [Ionic CLI](https://github.com/ionic-team/ionic-cli)

Example:

```sh
$ ionic g provider name-service
```

and add in `/src/app/app.module.ts`

## Language use

Use [@ngx-translate/core](https://github.com/ngx-translate/core) v9.1.1

```sh
$ npm install @ngx-translate/core@9.1.1 --save
```
or 

```sh
$ yarn add @ngx-translate/core@9.1.1
```

use translateService into view

```html
<div>{{ 'hello' | translate }}</div>
```

define translate into `/assets/i18n/es.json` or `/assets/i18n/en.json`

```json
{
    "HOME": {
        "HELLO": "hello"
    }
}
```

