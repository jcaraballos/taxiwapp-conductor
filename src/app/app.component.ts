import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';

import { HomePage } from '../pages/home/home';
import { LanguageSelectPage } from '../pages/language-select/language-select';

import { AuthProvider } from '../providers/auth/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage:any = HomePage;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private translate: TranslateService,
    private events: Events,
    private auth: AuthProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.initializeApp();

    });
  }

  initializeApp() {
    this.translate.setDefaultLang('es');
     if (localStorage.getItem('lang')) {
       this.translate.use(localStorage.getItem('lang'));
    }

    if (this.auth.getCurrentUser()) {
      this.rootPage = HomePage;
    } else {
      this.rootPage = LanguageSelectPage;
    }

    this.listenToLoginEvents();
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      // Si hay un logueo de cuenta o cambio de usuario se redirige a Home
      setTimeout(() => this.nav.setRoot(HomePage));
    });
    this.events.subscribe('user:notAuthenticated', () => {
      setTimeout(() => this.nav.setRoot(LanguageSelectPage));
    });

    this.events.subscribe('user:signup', () => {
    });

    this.events.subscribe('user:logout', () => {
      this.nav.setRoot(LanguageSelectPage);
    });
  }

}

