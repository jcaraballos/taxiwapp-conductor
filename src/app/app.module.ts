import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { GooglePlus } from '@ionic-native/google-plus';
import { Geolocation } from '@ionic-native/geolocation';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { AuthPage } from '../pages/auth/auth';
import { LanguageSelectPage } from '../pages/language-select/language-select';
import { RecoverPasswordPage } from '../pages/recover-password/recover-password';

const pages = [
  HomePage,
  LanguageSelectPage,
  LoginPage,
  AuthPage,
  RecoverPasswordPage,
];

import { LanguageProvider } from '../providers/language/language';
import { AuthProvider } from '../providers/auth/auth';
import { BaseProvider } from '../providers/base/base';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    ...pages
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {}, {
      links: [
        { component: HomePage, name: 'HomePage', segment: 'home' },
        { component: LanguageSelectPage, name: 'LanguageSelectPage', segment: 'lang' },
        { component: LoginPage, name: 'LoginPage', segment: 'login' },
        { component: AuthPage, name: 'AuthPage', segment: 'auth' },
        { component: RecoverPasswordPage, name: 'RecoverPasswordPage', segment: 'recover-password' },
      ]
    }),
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ...pages
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LanguageProvider,
    AuthProvider,
    GooglePlus,
    Geolocation,
    BaseProvider,
  ]
})
export class AppModule {}
