import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Events} from 'ionic-angular';

import { Geolocation, Geoposition } from '@ionic-native/geolocation';

import { GooglePlus } from '@ionic-native/google-plus';

import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

declare var google;

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  private currentUser: any;
  private token: string;
  private ip: string;
  private position: string;

  constructor(
    public http: HttpClient,
    private googlePlus: GooglePlus,
    private events: Events,
    public geolocation: Geolocation
  ) {
    this.currentUser = localStorage.getItem('idUsuario');
    this.token = localStorage.getItem('token');
    this.getIpAddress();
    this.getPosition();
  }

  public getCurrentUser(): string{
    return localStorage.getItem('idUsuario');
  }

  public getToken(): string{
    return localStorage.getItem('token');
  }

  public getCurrentPosition(): string{
    return this.position;
  }

  public getIp(): string{
    return this.ip;
  }

  getPosition(){
    let latitude: number;
    let longitude: number;
    this.geolocation.getCurrentPosition().then(response => {
      this.setPosition(response);
    })
    .catch(error =>{
      if (navigator) {
        navigator.geolocation.getCurrentPosition(response => {
          this.setPosition(response);
        });
      } else {
        if(google.loader.ClientLocation) {
          latitude = google.loader.ClientLocation.latitude;
          longitude = google.loader.ClientLocation.longitude;
          this.position = longitude +' '+ latitude;
        }
      }
    })
  }

  setPosition(response){
    let latitude = response.coords.latitude;
    let longitude = response.coords.longitude;
    this.position = longitude +' '+ latitude;
  }

  getIpAddress() {
    return this.http.get('http://api.ipify.org/?format=json')
      .toPromise()
      .then((response: any) => this.ip = response.ip );
  }

  login(email: string, pass: string): Observable<any>{
    const data = {
      "tipoConexion": 'local',
      "email": email,
      "pass": pass,
      "ubicacionGPS": this.getCurrentPosition(),
      "ipOrigen": this.getIp()
    }
    return this.http.post(`${environment.api}/bl_usuarios_iniciosesion`, data)
  }

  loginGoogle(){
    return this.googlePlus.login({});
  }

  recoverPassword(email: string): Observable<any>{
    const data = {
      "email": email,
      "idUsuario": 1,
      "Idioma": localStorage.getItem('lang')=='es'?'esp':"eng"
    }
    return this.http.post(`${environment.api}/st_usuarios_recuperar`, data)
  }

  guardAuthenticated() {
    if (this.getCurrentUser()) {
      return true;
    } else {
      this.events.publish('user:notAuthenticated');
      return false;
    }
  }

  guardNotAuthenticated() {
    if (this.getCurrentUser()) {
      this.events.publish('user:login');
      return false;
    } else {
      return true;
    }
  }

}
