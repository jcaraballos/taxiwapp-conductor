export const environment = {
  production: false,
  api: "http://app.exitum.us:9080/api",
  documents: "http://app.exitum.us:9080/documentos/",
};
