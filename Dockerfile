#Download base image ubuntu 16.04
FROM ubuntu:16.04

# Update Ubuntu Software repository
RUN apt-get update

RUN apt-get install -y -q \
    curl

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -

# nodejs includes matching npm as well
RUN apt-get install -y -q \
    nodejs \
    && apt-get -y autoclean \
    && rm -rf /var/lib/apt/lists/*

RUN npm install -g -y ionic


CMD bash -C '/start.sh';'bash'

EXPOSE 8100 35729